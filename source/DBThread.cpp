#include "DBThread.h"

DBThread::DBThread() {
	QueryQueue = new Queue(100000);
	sem = new ost::Semaphore(1);
	db = new func_db(0);
	Running = true;
	debug = false;

	if(!((con = db->connect("localhost", "IndeXor", "scrubbed")) > 0)) {
		cout << "Could not connect to db.\n";
		exit();
	}
	db->use_database("IndeXor");

}

void DBThread::Add(QueryNode *node) {
	sem->wait();
	while(QueryQueue->IsFull()) { usleep(10); }
	QueryQueue->Push(node);
	sem->post();
}

void DBThread::Finished() {
	Running = false;
}


void DBThread::run() {

	if(debug) cout << "The db thread is start\n";

	while(Running || !QueryQueue->IsEmpty()) {

		if(!QueryQueue->IsEmpty()) {
			QueryNode *working = (QueryNode *)QueryQueue->Pop();
			//working->Print();
			working->result = db->query(working->Query);
			if(working->Type == INSERT_QUERY) { working->Index = db->get_index(); }
			if(working->CanDelete) {
				db->free_result(working->result);
				delete working;
			}
			else working->Completed = true;
		} else {
			usleep(10);
		}
	}
	if(debug) cout << "DB Thread has finished." << endl;
	return;
}
